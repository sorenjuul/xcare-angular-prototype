'use strict';

/**
 * @ngdoc service
 * @name xApp.callForwarding
 * @description
 * # callForwarding
 * Factory in the xApp.
 */
angular.module('xApp')
  .factory('callForwarding', function ($timeout) {
    // Service logic
    // ...

    var settings = {
      forwardCalls:true,
      whichCallsToForward:'all',
      forwardCallsTo:'phoneNumber',
      phoneNumber:'55667787'
    };

    // Public API here
    return {
      getSettings: function () {
        return $timeout(function(){
          return settings;
        }, 500);
      },
      saveSettings: function(){
        return $timeout(function(){
          return {msg: 'success'};
        }, 500);
      }
    };
  });
