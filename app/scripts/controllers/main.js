'use strict';

/**
 * @ngdoc function
 * @name xApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the xApp
 */
angular.module('xApp')
  .controller('MainCtrl', function ($scope) {

    $scope.groups = [
      {
        title: 'Udeladt nummer',
        content: 'Panel content 4'
      }
    ];

    $scope.status = {
      isFirstOpen: true,
      isFirstDisabled: false
    };
  });
