'use strict';

/**
 * @ngdoc function
 * @name xApp.controller:ForwardCtrl
 * @description
 * # ForwardCtrl
 * Controller of the xApp
 */
angular.module('xApp')
  .controller('ForwardCtrl', function ($scope, $timeout, callForwarding) {
    $scope.settings = null;


    $scope.callForwardClick = function(){
      console.log('Testing');
      callForwarding.getSettings().then(
        function(promise){
          $scope.status.isCallForwardDataLoaded = true;
          console.log(promise);
        }
      );
    };

    $scope.saveClick = function(){
      callForwarding.saveSettings().then(
        function(promise){
          if(promise.msg === 'success'){
            $scope.status.alert = true;
            $timeout(function(){
              $scope.status.alert = false;
            }, 2000);
          }
        }
      );
    };

    $scope.isDropDownSelected = function(){
      return $scope.status.whichCallsToForward !== 'none';
    };

    $scope.isForwardedToOtherNumber = function(){
      return $scope.status.forwardToSelect === 'phoneNumber';
    };

    $scope.canSave = function(){
      return $scope.status.forwardToSelect === 'voiceMail' || $scope.status.phoneNumber.length === 8;
    };

    $scope.status = {
      isFirstOpen: true,
      isFirstDisabled: false,
      isCallForwardDataLoaded: false,
      allowCallForward: false,
      whichCallsToForward: 'none',
      forwardTo:false,
      forwardToSelect: 'none',
      phoneNumber: '',
      alert:false
    };
  });
