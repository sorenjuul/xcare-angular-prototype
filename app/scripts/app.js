'use strict';

/**
 * @ngdoc overview
 * @name xApp
 * @description
 * # xApp
 *
 * Main module of the application.
 */
angular
  .module('xApp', [
    'ngRoute',
    'ui.bootstrap'
  ])
  .config(function ($routeProvider) {
    $routeProvider
      .when('/', {
        templateUrl: 'views/main.html',
        controller: 'MainCtrl'
      })
      .otherwise({
        redirectTo: '/'
      });
  });
