# README #

### What is this repository for? ###

* Prototyping with AngularJS

### How do I get set up? ###

```
#!shell
$ npm install -g bower grunt-cli
$ npm install
$ bower install

```
## Build & development

Run `grunt` for building and `grunt serve` for preview.

## Testing

Running `grunt test` will run the unit tests with karma.